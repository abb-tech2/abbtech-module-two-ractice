package org.abbtech.finalProjectModul2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
    private static final String JDBC_URL = "jdbc:postgresql://127.0.0.1:5432/module2";
    private static final String USER_NAME = "revan";
    private static final String PASS = "revan";

    public static Connection getConnection() {
        Connection connection;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;
    }

}
