package org.abbtech.lesson4;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "UserLoginServlet", urlPatterns = "/user-login")
public class UserLoginServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = "username";
        String password = "password";
        var writer = resp.getWriter();

        String actualUsername = req.getParameter("user");
        String actualPass = req.getParameter("pass");

        if(username.equalsIgnoreCase(actualUsername) && password.equalsIgnoreCase(actualPass)){
            Cookie cookie = new Cookie("username", actualUsername);
            cookie.setMaxAge(300);
            resp.addCookie(cookie);
            writer.write("User login with username: " + actualUsername);
        }


    }
}
