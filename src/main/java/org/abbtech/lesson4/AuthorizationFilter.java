package org.abbtech.lesson4;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;

import java.io.IOException;

@WebFilter(filterName = "AuthorizationFilter", urlPatterns = "/user/profile")
public class AuthorizationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        System.out.println("log in");
        filterChain.doFilter(servletRequest, servletResponse);
        
    }
}
