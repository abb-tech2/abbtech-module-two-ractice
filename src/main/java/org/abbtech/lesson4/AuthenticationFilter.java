package org.abbtech.lesson4;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.net.http.HttpRequest;

@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/user/profile")
public class AuthenticationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        System.out.println("log in");
        HttpServletRequest reqs = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        var cks = reqs.getCookies();


        if(cks!=null && cks.length > 0 && cks[0].getName()!= null){
            filterChain.doFilter(servletRequest, servletResponse);
        }else{
            resp.setStatus(401);
            resp.getWriter().write("you are not login");
        }

    }
}
