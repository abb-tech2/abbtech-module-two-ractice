package org.abbtech.lesson2.classwork;

public class ApplicationService {
    private final CalculatorService calculatorService;
    public ApplicationService(CalculatorService calculatorService){
        this.calculatorService = calculatorService;
    }

    public int multiply(int a, int b){
        if(a > 4 && b > 6){
            throw new ArithmeticException("a is grt than 4, b is grt than 6");
        }
        return calculatorService.multiply(a, b);
    }

    public int substraction(int a, int b){
        var result = calculatorService.substract(a, b);
        if(result == 2 || result == 4 || result == 6){
            throw new ArithmeticException("2, 4, 6 is not allowed");
        }
        return result;
    }
    public float division(int a, int b){
        var result = calculatorService.division(a, b);
        if(a % 2 == 1 || b % 2 == 1){
            throw new ArithmeticException("at least one of this is odd number, is not allowed");
        }
        if (b == 0) {
            throw new ArithmeticException("Division by zero is not allowed");
        }
        return result;
    }
    public int addition(int a, int b){
        var result = calculatorService.addition(a, b);
        if(a < 0 || b < 0){
            throw new ArithmeticException("minus numbers is not allowed");
        }
        return result;
    }


}
