package org.abbtech.lesson2.classwork;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.abbtech.lesson3.CalculationRepository;
import org.abbtech.lesson3.JDBCConnection;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/add", "/calculator/multiply", "/calculator/substract", "/calculator/divide"})
public class CalculationServlet extends HttpServlet {
    private ApplicationService applicationService;
    private CalculationRepository calculationRepository;

    @Override 
    public void init() throws ServletException {
        super.init();
        applicationService = new ApplicationService(new CalculatorServiceImp());
        calculationRepository = new CalculationRepository();
        System.out.println("init method");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String calculationMethod = "multiply"; /*req.getHeader("x-calculation-method");*/
        int a = Integer.parseInt(req.getParameter("a"));
        int b = Integer.parseInt(req.getParameter("b"));
        int result = 0;

        PrintWriter writer = resp.getWriter();
        String customAttribute = (String) req.getAttribute("x-filter-name");
        HttpSession httpSession = req.getSession(false);
        httpSession.setAttribute("session-use", "session-user");
        String string = httpSession.getId();

        var cks = req.getCookies();
        String userDefined = cks[0].getValue();

        try {
            switch (calculationMethod.toLowerCase()) {
                case "multiply":
                    result = applicationService.multiply(a, b);
                    break;
                case "substract":
                    result = applicationService.substraction(a, b);
                    break;
                case "add":
                    result = applicationService.addition(a, b);
                    break;
                case "divide":
                    result = (int) applicationService.division(a, b);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid calculation method");
            }

//            try {
//                calculationRepository.saveCalculationResult(a, b, result, calculationMethod);
//            } catch (SQLException e) {
//                throw new RuntimeException(e);
//            }
            var calculations = calculationRepository.getCalculationResult();
            writer.write("""
                    {
                        "result": ok
                        
                    }
                    """);

            Cookie cookieUser = new Cookie("user-name", "guest");
            Cookie cookieUserRole = new Cookie("user-role", "guest-role");

            cookieUserRole.setMaxAge(300);
            cookieUser.setMaxAge(300);

            resp.addCookie(cookieUser);
            resp.addCookie(cookieUserRole);

        } catch (ArithmeticException ex) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("Invalid arguments");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        writer.close();

        resp.setContentType("application/json");


        super.doGet(req, resp);
    }
    // cliente nese demek uchun istifade olunur


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();
        int readLine;
        while ((readLine = reader.read()) != 1) {
            char ch = (char) readLine;
        }
        writer.close();
    }
    // her defe servlet proyekti qalxanda bir defe ishleyir


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int id = Integer.parseInt(req.getParameter("id"));
        PrintWriter writer = resp.getWriter();

        try {

            try {
                calculationRepository.deleteCalculationResult(id);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            writer.write("""
                    {
                        "result": ok
                        
                    }
                    """);

        } catch (Exception exception){
            exception.getMessage();
        }
        writer.close();

        resp.setContentType("application/json");

        super.doDelete(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int a = Integer.parseInt(req.getParameter("a"));
        int b = Integer.parseInt(req.getParameter("b"));
        int id = Integer.parseInt(req.getParameter("id"));
        int result = 0;

        PrintWriter writer = resp.getWriter();

        try {


            calculationRepository.updateCalculationResult(id, a, b);
            var calculations = calculationRepository.getCalculationResult();
            writer.write("""
                    {
                        "result": """ + "ok" + """
                        
                    }
                    """);

        } catch (ArithmeticException ex) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("Invalid arguments");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        writer.close();

        resp.setContentType("application/json");


        super.doGet(req, resp);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
        System.out.println("service method");
    }
    // her defe http methodlarini cagirdigda ishleyir (post, get kimi)


    @Override
    public void destroy() {
        super.destroy();
        System.out.println("destroy method");
    }
    //sevlet down olduqda ishleyir
}
