package org.abbtech.lesson2.classwork;

public interface CalculatorService {
    int multiply(int i, int j);
    int substract(int i, int j);
    int addition(int i, int j);
    float division(int i, int j);
}
