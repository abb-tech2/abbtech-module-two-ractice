package org.abbtech.lesson2.classwork;

public class CalculatorServiceImp implements CalculatorService{
    @Override
    public int multiply(int i, int j) {
        return i * j;
    }

    @Override
    public int substract(int i, int j) {
        return i - j;
    }

    @Override
    public int addition(int i, int j) {
        return i + j;
    }
    @Override
    public float division(int i, int j) {
        return (float) i / j;
    }
}
