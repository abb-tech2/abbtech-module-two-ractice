package org.abbtech.lesson3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:6432/abb_tech";
    private static final String USER_NAME = "psg_user";
    private static final String PASSWORD = "pass";
    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Unable to load PostgreSQL JDBC driver", e);
        }
    }
    public static Connection getConnection() throws ClassNotFoundException, SQLException {

        Connection connection;

        Class.forName("org.postgresql.Driver");

        connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASSWORD);

        return connection;
    }
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Connection c=JDBCConnection.getConnection();
    }
}
