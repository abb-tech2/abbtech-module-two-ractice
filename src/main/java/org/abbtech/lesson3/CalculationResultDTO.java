package org.abbtech.lesson3;

public record CalculationResultDTO(int a, int b, int result, String method) {

}
