package org.abbtech.lesson3;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CalculationRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:6432/abb_tech";
    private static final String USER_NAME = "psg_user";
    private static final String PASSWORD = "pass";


    public void saveCalculationResult(int a, int b, int result, String method) throws SQLException {
        Connection connection = null;
        String sqlInsert = """
                insert into "CALCULATION".calculation_result (variable_a, variable_b, calc_result, calc_method)\s
                values (?, ?, ?, ?)
                """;
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }

            try {
                connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASSWORD);
                connection.setAutoCommit(false);
                PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
                preparedStatement.setInt(1,a);
                preparedStatement.setInt(2,b);
                preparedStatement.setInt(3,result);
                preparedStatement.setString(4,method);
                preparedStatement.execute();
                connection.commit();


            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            finally {
                if(connection != null && !connection.isClosed()){
                    connection.close();
                }
            }
    }

    public void updateCalculationResult(int id, int variable_a, int variable_b) throws SQLException {
        Connection connection = null;
        String sqlUpdate = """
                update "CALCULATION".calculation_result set variable_a = ?, variable_b = ? where id = ?;
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASSWORD);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setInt(1,variable_a);
            preparedStatement.setInt(2,variable_b);
            preparedStatement.setInt(3,id);
            preparedStatement.execute();
            connection.commit();


        } catch (SQLException e) {

            connection.rollback();
            throw new RuntimeException(e);
        }
        finally {
            if(connection != null && !connection.isClosed()){
                connection.close();
            }
        }
    }


    public void deleteCalculationResult(int id) throws SQLException {
        Connection connection = null;
        String sqlDelete = """
                delete from "CALCULATION".calculation_result where ID = ?;
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASSWORD);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setInt(1,id);
            preparedStatement.execute();
            connection.commit();


        } catch (SQLException e) {

            connection.rollback();
            throw new RuntimeException(e);
        }
        finally {
            if(connection != null && !connection.isClosed()){
                connection.close();
            }
        }
    }

    public List<CalculationResultDTO> getCalculationResult() throws SQLException {
        Connection connection = null;
        List<CalculationResultDTO> calculations = new ArrayList<>();
        String sqlSelect = """
                select * from "CALCULATION".calculation_result;
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASSWORD);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
            ResultSet calculationResult = preparedStatement.executeQuery();
            while (calculationResult.next()){
                int a = calculationResult.getInt("VARIABLE_A");
                int b = calculationResult.getInt("VARIABLE_B");
                int result = calculationResult.getInt("CALC_RESULT");
                String calcMethod = calculationResult.getString("CALC_METHOD");

                calculations.add(new CalculationResultDTO(a, b, result, calcMethod));
            }
            return calculations;

        } catch (SQLException e) {

            connection.rollback();
            throw new RuntimeException(e);
        }
        finally {
            if(connection != null && !connection.isClosed()){
                connection.close();
            }
        }

    }
}
