package org.abbtech.lesson2.Exercise14Test;

import org.abbtech.lesson2.classwork.ApplicationService;
import org.abbtech.lesson2.classwork.CalculatorService;
import org.abbtech.lesson2.classwork.CalculatorServiceImp;
import static org.junit.jupiter.api.Assertions.*;

import org.abbtech.lesson2.exercises.UserServiceImp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

class CalculationServletTest {

    @Mock
    private CalculatorService calculatorService;
    @InjectMocks
    private ApplicationService applicationService;

    @BeforeEach
    void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void multiplication_PositiveTest(){
        when(calculatorService.multiply(4, 5)).thenReturn(20);
        assertEquals(20, applicationService.multiply(4, 5));
        verify(calculatorService).multiply(4, 5);
    }

    @Test
    void multiplication_NegativeTest(){
        when(calculatorService.multiply(6, 7)).thenReturn(42);
        assertEquals(42, applicationService.multiply(6, 7));
        verify(calculatorService).multiply(6, 7);
    }

    @Test
    void substraction_PositiveTest(){
        when(calculatorService.substract(7, 5)).thenReturn(2);
        assertEquals(2, applicationService.substraction(7, 5));
        verify(calculatorService).substract(7, 5);
    }

    @Test
    void substraction_NegativeTest(){
        when(calculatorService.substract(6, 4)).thenReturn(2);
        assertEquals(2, applicationService.substraction(6, 4));
        verify(calculatorService).substract(6, 4);
    }

    @Test
    void addition_PositiveTest(){
        when(calculatorService.addition(1, 2)).thenReturn(3);
        assertEquals(3, applicationService.addition(1, 2));
        verify(calculatorService).addition(1, 2);
    }

    @Test
    void addition_NegativeTest(){
        when(calculatorService.addition(-1, -2)).thenReturn(-3);
        assertEquals(-3, applicationService.addition(-1, -2));
        verify(calculatorService).addition(-1, -2);
    }

    @Test
    void division_PositiveTest(){
        when(calculatorService.division(4, 2)).thenReturn(2F);
        assertEquals(2, applicationService.division(4, 2));
        verify(calculatorService).division(4, 2);
    }

    @Test
    void division_NegativeTest(){
        when(calculatorService.division(3, 2)).thenReturn(1.5F);
        assertEquals(1.5, applicationService.division(3, 2));
        verify(calculatorService).division(3, 2);
    }


    //exception testing part below:

    @Test
    void testSubtractionWithNotAllowedResult() {

        when(calculatorService.substract(6, 4)).thenReturn(2);
        assertThrows(ArithmeticException.class, () -> applicationService.substraction(6, 4));
    }

    @Test
    void testDivisionByZero() {
        assertThrows(ArithmeticException.class, () -> applicationService.division(5, 0));
    }

    @Test
    void testDivisionWithOddNumbers() {
        assertThrows(ArithmeticException.class, () -> applicationService.division(5, 3));
    }

    @Test
    void testAdditionWithNotAllowedResult(){
        when(calculatorService.addition(-1, -2)).thenReturn(-3);
        assertThrows(ArithmeticException.class, () -> applicationService.addition(-1, -2)) ;
    }

    @Test
    void testMultiplicationWithNotAllowedResult(){
        when(calculatorService.multiply(6, 7)).thenReturn(42);
        assertThrows(ArithmeticException.class, () -> applicationService.multiply(6, 7));
    }

//    Bunlari birinci yazmishdim, silmeye elim gelmedi ;(

//    CalculatorService calculatorService = new CalculatorServiceImp();
//    ApplicationService applicationService = new ApplicationService(calculatorService);
//
//    @Test
//    void multiplication_PositiveTest(){
//        assertEquals(20, applicationService.multiply(2, 10));
//    }
//
//    @Test
//    void substraction_PositiveTest(){
//        assertEquals(3, applicationService.substraction(5, 2));
//    }
//
//    @Test
//    void division_PositiveTest(){
//        assertEquals(3, applicationService.division(6, 2));
//    }
//
//    @Test
//    void addition_PositiveTest(){
//        assertEquals(3, applicationService.addition(1, 2));
//    }
//
//    @Test
//    void addition_NegativeTest(){
//        assertEquals(-3, applicationService.addition(-1, -2));
//    }
//
//    @Test
//    void division_NegativeTest(){
//        assertEquals(3, applicationService.division(9, 3));
//    }
//
//    @Test
//    void substraction_NegativeTest(){
//        assertEquals(4, applicationService.substraction(8, 4));
//    }
//
//    @Test
//    void multiplication_NegativeTest(){
//        assertEquals(72, applicationService.multiply(8, 9));
//    }
//
//    @Test
//    public void testDivisionByZero() {
//        assertThrows(ArithmeticException.class, () -> applicationService.division(5, 0));
//    }


}
