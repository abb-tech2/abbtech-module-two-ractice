package org.abbtech.lesson2.classwork;

import org.abbtech.lesson2.classwork.ApplicationService;
import org.abbtech.lesson2.classwork.CalculatorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class    ApplicationServiceUnitTest {
    @Mock
    private CalculatorService calculatorService;
    @InjectMocks
    private ApplicationService applicationService;

    @Test
    void multiplySuccess(){
        when(calculatorService.multiply(anyInt(), anyInt())).thenReturn(12);
        int actual = applicationService.multiply(3, 4);
        verify(calculatorService, times(1)).multiply(3, 4);
        Assertions.assertEquals(12, actual);
    }
    @Test
    void substractSuccess(){
        when(calculatorService.substract(anyInt(), anyInt())).thenReturn(7);
        int actual = applicationService.substraction(10, 3);
        verify(calculatorService, times(1)).substract(10, 3);
        Assertions.assertEquals(7, actual);
    }

    @ParameterizedTest
    @CsvSource(value = {"4,2,2", "8,4,4", "10,4,6", "10,2,8"}, delimiter = ',')
    void substractThrows(int a, int b, int result){
        when(calculatorService.substract(a, b)).thenReturn(result);
        Exception exception = Assertions.assertThrows(ArithmeticException.class,
                ()-> applicationService.substraction(a, b));

        Assertions.assertInstanceOf(ArithmeticException.class, exception);
        verify(calculatorService, times(1)).substract(10, 3);
    }

    @ParameterizedTest
    @CsvSource(value = {"5,7", "6,8", "10,20"}, delimiter = ',')
    void multiplyThrows(int a, int b){
        Exception exception = Assertions.assertThrows(ArithmeticException.class,
                ()-> applicationService.multiply(a, b));
        Assertions.assertInstanceOf(ArithmeticException.class, exception);
    }
}
