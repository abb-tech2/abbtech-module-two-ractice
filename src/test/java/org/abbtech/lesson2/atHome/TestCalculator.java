//package org.abbtech.lesson2.atHome;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import org.abbtech.lesson2.classwork.CalculatorService;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnit;
//import org.mockito.junit.MockitoRule;
//
//import static org.mockito.Mockito.*;
////@ExtendWith(MockitoExtension.class)
//public class TestCalculator {
//
//    Calculator c = null;
////    CalculatorService service = new CalculatorService() {
////        @Override
////        public int add(int i, int j) {
////            return 0;
////        }
////    };          //this is fake object and we call it mock
////                //instead of using this anom class we use mockito framework
//
//    @Mock
//    CalculatorService service;
//
//    @Rule
//    public MockitoRule rule = MockitoJUnit.rule();
//
//    @BeforeEach
//    void  setUp(){
//        c = new Calculator(service);
//    }
//
//    @Test
//    void testPerform(){
//        when(service.add(2,3)).thenReturn(5);
//        assertEquals(10, c.perform(2, 3));
//    }
//}
